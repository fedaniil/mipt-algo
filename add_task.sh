#!/bin/sh
# Usage: ./add_task.sh 5 B ../mipt-algo-play/5/B.cpp
git checkout master
branch="mod$1_task$2"
git checkout -b "$branch"
echo ___ CREATE FILE ON "$(git branch --show-current)" ___
mkdir -p "./mod$1/task$2"
file="./mod$1/task$2/main.cpp"
touch "$file"
cat "$3" > "$file"
echo ___ FILE CONTENT AT "$file" ___
cat "$file"
echo '>>> COMMIT? (y/n) '
read -r;
if [ "$REPLY" != "y" ]
then
	git checkout master
	git branch -D "$branch"
	exit 1
fi
git add "$file"
git commit -m "Add $branch"
echo '>>> PUSH? (y/n) '
read -r;
if [ "$REPLY" != "y" ]
then
	git checkout master
	git branch -D "$branch"
	exit 1
fi
git push -u origin "$branch"
echo "<<< PR NAME [Фёдоров Даниил] Модуль$1-Задача$2-"
printf "<<< PR DESC\nOK во время контеста: \nOK версии из git: "
