#include <iostream>
#include <vector>
#include <random>

int randint(int from, int to) {  // [from, to]
    std::random_device rand_dev;
    std::mt19937 gen(rand_dev());
    std::uniform_int_distribution<> distr(from, to);
    return distr(gen);
}

int partition(std::vector<int> &arr, int l, int r) {
    std::swap(arr[randint(l, r)], arr[r]);
    int pivot = arr[r];
    int fir_greater = l;
    for (int i = l; i < r; ++i) {
        if (arr[i] <= pivot) {
            std::swap(arr[i], arr[fir_greater]);
            ++fir_greater;
        }
    }
    std::swap(arr[fir_greater], arr[r]);
    return fir_greater;
}

int kth_elem(std::vector<int> &arr, int k, int l, int r) {
    while (true) {
        int m = partition(arr, l, r);

        if (m == k) {
            return arr[m];
        } else if (k < m) {
            r = m - 1;
        } else {
            l = m + 1;
        }
    }
}

void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n, k;
    cin >> n >> k;

    std::vector<int> arr(n);
    for (int &x : arr) cin >> x;

    cout << kth_elem(arr, k, 0, arr.size() - 1) << '\n';

    return 0;
}