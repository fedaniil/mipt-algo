#include <iostream>
#include <vector>

unsigned long long get_byte(unsigned long long x, unsigned int byte) {
    return (x >> (8 * byte)) & 255u;
}

inline void lsd_sort(std::vector<unsigned long long> &arr, int n) {
    int cnt[1u << 8u], shift[1u << 8u];
    std::vector<unsigned long long> new_arr(n);
    for (unsigned int byte = 0; byte < 8; ++byte) {
        std::fill_n(cnt, 1u << 8u, 0);
        for (int i = 0; i < n; ++i) {
            ++cnt[get_byte(arr[i], byte)];
        }
        int cur_shift = 0;
        for (int i = 0; i < (1u << 8u); ++i) {
            shift[i] = cur_shift;
            cur_shift += cnt[i];
        }
        for (int i = 0; i < n; ++i) {
            int num_byte = get_byte(arr[i], byte);
            new_arr[shift[num_byte]] = arr[i];
            ++shift[num_byte];
        }
        arr = new_arr;
    }
}

void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n;
    cin >> n;
    std::vector<unsigned long long> arr(n);
    for (unsigned long long &x : arr) cin >> x;

    lsd_sort(arr, n);
    for (unsigned long long &x : arr) cout << x << ' ';

    return 0;
}