#include <iostream>
#include <cassert>
#include <vector>

typedef long long ll;

class Binary_heap {
    std::vector<ll> pool;
    int size = 0;

    void sift_up(int i) {
        while (pool[i] < pool[(i - 1) / 2]) {
            std::swap(pool[i], pool[(i - 1) / 2]);
            i = (i - 1) / 2;
        }
    }

    void sift_down(int i) {
        while (2 * i + 1 < size) {
            int left = 2 * i + 1;
            int right = 2 * i + 2;
            int min_son = left;
            if (right < size && pool[right] < pool[left]) {
                min_son = right;
            }
            if (pool[i] <= pool[min_son]) {
                break;
            } else {
                std::swap(pool[i], pool[min_son]);
                i = min_son;
            }
        }
    }

public:

    ll get_min() {
        assert(size > 0);
        return pool[0];
    }

    void extract_min() {
        assert(size > 0);
        pool[0] = pool[size - 1];
        sift_down(0);
        pool.pop_back();
        --size;
    }

    void insert(ll x) {
        pool.emplace_back(x);
        ++size;
        sift_up(size - 1);
    }

    bool self_check(int i) {
        if (i > size - 1)
            return true;
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        if (left < size && pool[i] > pool[left])
            return false;
        if (right < size && pool[i] > pool[right])
            return false;
        return self_check(left) && self_check(right);
    }
};

void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n, k;
    cin >> n >> k;

    Binary_heap first_k;
    ll x;
    for (int i = 0; i < std::min(n, k); ++i) {
        cin >> x;
        first_k.insert(-x);
    }
    for (int i = std::min(n, k); i < n; ++i) {
        cin >> x;
        first_k.insert(-x);
        first_k.extract_min();
    }

    std::vector<ll> ans;
    for (int i = 0; i < std::min(n, k); ++i) {
        ans.emplace_back(-first_k.get_min());
        first_k.extract_min();
    }
    for (int i = ans.size() - 1; i >= 0; --i) {
        cout << ans[i] << ' ';
    }

    return 0;
}