#include <iostream>
#include <string>

class BigInteger {
public:

    BigInteger() = default;

    BigInteger(const BigInteger& big_n) = default;

    BigInteger(long long n): number(std::to_string(std::abs(n))), inv(n < 0) {}

    BigInteger& operator=(BigInteger big_n) {
        swap(big_n);
        return *this;
    }

    explicit operator bool() const {
        return number != "0";
    }

    std::string toString() const {
        return inv ? '-' + number : number;
    }

    int count_digits() const {
        return number.size();
    }

    int get_digit(size_t i) const {
        return number[i] - '0';
    }

    BigInteger operator+() const {
        return *this;
    }

    BigInteger operator-() const;

    BigInteger& operator+=(const BigInteger& big_n);

    BigInteger& operator-=(const BigInteger& big_n);

    BigInteger& operator++() {
        return *this += 1;
    }

    BigInteger operator++(int);

    BigInteger& operator--() {
        return *this -= 1;
    }

    BigInteger operator--(int);

    BigInteger& operator*=(const BigInteger& big_n);

    BigInteger& operator/=(const BigInteger& big_n) {
        divmod(big_n, "div");
        return *this;
    }

    BigInteger& operator%=(const BigInteger& big_n) {
        divmod(big_n, "mod");
        return *this;
    }

    // для удобства
    int mod10() const {
        return get_digit(number.size() - 1);
    }

    // для уменьшения кол-ва друзей; для быстрого сравнения с 0
    bool get_inv() const {
        return inv;
    }

    friend std::istream& operator>>(std::istream&, BigInteger&);  // вынужденный друг
    friend BigInteger operator"" _big(const char*, size_t);  // так сильно удобнее

private:

    std::string number = "0";
    bool inv = false;

    // для удобства
    BigInteger(std::string s, bool inv = false): number(std::move(s)), inv(inv) {
        remove_leading_zeros();
    }

    void swap(BigInteger& big_n) {
        std::swap(number, big_n.number);
        std::swap(inv, big_n.inv);
    }

    void remove_leading_zeros();

    // выполняет деление с остатком, записывает по месту результат деления/остаток (mode == "div" / "mod" соотв)
    void divmod(const BigInteger& big_n, const std::string& mode);
};

bool operator<(const BigInteger& a, const BigInteger& b) {
    if (a.get_inv() && b.get_inv()) {  // -|a| < -|b|
        return -b < -a;  // |b| < |a|
    } else if (!a.get_inv() && b.get_inv()) {  // |a| < -|b|
        return false;
    } else if (a.get_inv() && !b.get_inv()) {  // -|a| < |b|
        return true;
    }
    // |a| < |b|
    if (a.count_digits() == b.count_digits()) {
        for (int i = 0; i < a.count_digits(); ++i) {
            if (a.get_digit(i) < b.get_digit(i)) {
                return true;
            } else if (a.get_digit(i) > b.get_digit(i)) {
                return false;
            }
        }
        return false;
    }
    return a.count_digits() < b.count_digits();
}

inline bool operator>(const BigInteger& a, const BigInteger& b) {
    return b < a;
}

inline bool operator<=(const BigInteger& a, const BigInteger& b) {
    return !(a > b);
}

inline bool operator>=(const BigInteger& a, const BigInteger& b) {
    return !(a < b);
}

inline bool operator==(const BigInteger& a, const BigInteger& b) {
    return !(a < b || a > b);
}

inline bool operator!=(const BigInteger& a, const BigInteger& b) {
    return !(a == b);
}

BigInteger BigInteger::operator-() const {
    BigInteger res(*this);
    if (res.number != "0") {
        res.inv ^= true;
    }
    return res;
}

BigInteger operator+(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res += b;
    return res;
}

BigInteger operator-(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res -= b;
    return res;
}

BigInteger operator*(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res *= b;
    return res;
}

BigInteger operator/(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res /= b;
    return res;
}

BigInteger operator%(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res %= b;
    return res;
}

BigInteger& BigInteger::operator+=(const BigInteger& big_n) {
    if (inv) {  // -|this| += big_n
        return *this = -(-*this - big_n);  // this = -(|this| - big_n)
    } else if (!inv && big_n.inv) {  // |this| += -|big_n|
        return *this -= -big_n;  // this -= |big_n|
    }
    // |this| += |big_n|
    std::string res(std::max(number.size(), big_n.number.size()) + 1, '0');
    int pos1 = number.size() - 1, pos2 = big_n.number.size() - 1, writepos = res.size() - 1;
    for (; writepos > 0; --pos1, --pos2, --writepos) {
        short digit1 = pos1 >= 0 ? number[pos1] - '0' : 0;
        short digit2 = pos2 >= 0 ? big_n.number[pos2] - '0' : 0;
        res[writepos] += digit1 + digit2;
        if (res[writepos] > '9') {
            res[writepos - 1] += 1;
            res[writepos] -= 10;
        }
    }
    number = res;
    remove_leading_zeros();
    return *this;
}

BigInteger& BigInteger::operator-=(const BigInteger& big_n) {
    if (inv) {  // -|this| -= big_n
        return *this = -(-*this + big_n);  // this = -(|this| + big_n)
    } else if (!inv && big_n.inv) {  // |this| -= -|big_n|
        return *this += -big_n;  // this += |big_n|
    }
    // |this| -= |big_n|
    if (big_n > *this) {
        return *this = -(big_n - *this);
    }
    bool taken_one = false;
    for (int pos1 = number.size() - 1, pos2 = big_n.number.size() - 1; pos1 >= 0; --pos1, --pos2) {
        short digit1 = number[pos1] - '0';
        short digit2 = pos2 >= 0 ? big_n.number[pos2] - '0' : 0;
        if (taken_one && digit1 > 0) {
            --digit1;
            taken_one = false;
        } else if (taken_one) {
            digit1 += 9;
        }
        number[pos1] = '0' + (digit1 - digit2);
        if (number[pos1] < '0') {
            number[pos1] += 10;
            taken_one = true;
        }
    }
    remove_leading_zeros();
    return *this;
}

BigInteger BigInteger::operator++(int) {
    BigInteger copy(*this);
    *this += 1;
    return copy;
}

BigInteger BigInteger::operator--(int) {
    BigInteger copy(*this);
    *this -= 1;
    return copy;
}

BigInteger& BigInteger::operator*=(const BigInteger& big_n) {
    std::string res(number.size() + big_n.number.size() + 1, '0');
    for (size_t pos2 = 0, shift = big_n.number.size() - 1; pos2 < big_n.number.size(); ++pos2, --shift) {
        int digit2 = big_n.number[pos2] - '0';
        int plus_next = 0;
        int writepos = res.size() - shift - 1;
        for (int pos1 = number.size() - 1; pos1 >= 0; --pos1, --writepos) {
            int digit1 = number[pos1] - '0';
            int sum = (res[writepos] - '0') + digit1 * digit2 + plus_next;
            res[writepos] = '0' + sum % 10;
            plus_next = sum / 10;
        }
        while (plus_next != 0) {
            int sum = (res[writepos] - '0') + plus_next;
            res[writepos] = '0' + sum % 10;
            plus_next = sum / 10;
            --writepos;
        }
    }
    number = res;
    remove_leading_zeros();
    inv ^= big_n.inv;
    if (number == "0") {
        inv = false;
    }
    return *this;
}

void BigInteger::remove_leading_zeros() {
    size_t fir = 0;
    while (fir < number.size() && number[fir] == '0') {
        ++fir;
    }
    if (fir == number.size()) {
        number = "0";
    } else {
        number = number.substr(fir);
    }
}

inline BigInteger abs(const BigInteger& big_n) {
    return big_n.get_inv() ? -big_n : big_n;
}

void BigInteger::divmod(const BigInteger& big_n, const std::string& mode) {
    size_t next_pos = big_n.number.size();
    std::string cur_mod = number.substr(0, next_pos), cur_div("0");
    while (true) {
        // в сравнениях строки и BigInteger используется приватный неявный каст строки к BigInteger
        while (next_pos < number.size() && cur_mod < abs(big_n)) {
            cur_mod += number[next_pos];
            cur_div += '0';
            ++next_pos;
        }
        if (cur_mod < abs(big_n)) {
            if (cur_mod == 0 && cur_mod.size() > 1) {  // если остаток - это 2 и больше нолей, то
                // число имело вид (x)0, где (x) получилось разделить нацело
                // предыдущий цикл не дописал 1 ноль т.к. он был "пропущен" в последнем условии вечного цикла,
                // которое "приписывает" к остатку следющий разряд, как при обычном делении столбиком
                cur_div += '0';
            }
            break;
        }
        int L = 1;
        int R = 10;
        while (R - L > 1) {
            int M = (R + L) / 2;
            if (abs(big_n) * M > cur_mod) {
                R = M;
            } else {
                L = M;
            }
        }
        cur_mod = (cur_mod - abs(big_n) * L).number; // здесь тоже используется неявный каст строки к BigInteger
        cur_div += '0' + L;
        if (next_pos < number.size()) {
            cur_mod += number[next_pos];
            ++next_pos;
        }
    }
    number = mode == "div" ? cur_div : cur_mod;
    if (mode == "div") {
        inv ^= big_n.inv;
    }
    if (number == "0") {
        inv = false;
    }
    remove_leading_zeros();
}

inline std::ostream& operator<<(std::ostream& out, const BigInteger& big_n) {
    return out << big_n.toString();
}

std::istream& operator>>(std::istream& in, BigInteger& big_n) {
    big_n = 0;
    std::string s;
    char c;
    do c = in.get(); while (isspace(c));  // пропустить ведущие пробелы
    // особая обработка первого символа
    bool inv = false;
    if (c == '-') {
        inv = true;
    } else {
        s = c;
    }
    // прочитать остальное
    for (c = in.get(); !in.eof() && !isspace(c); c = in.get()) {
        s += c;
    }

    big_n = BigInteger(s, inv);
    return in;
}

BigInteger operator"" _big(const char* x, size_t) {
    std::string s = x;
    if (s.empty()) return 0;
    return BigInteger(s[0] == '-' ? s.substr(1) : s, s[0] == '-');
}

BigInteger pow(BigInteger base, BigInteger deg) {
    BigInteger res(1);
    while (deg > 0) {
        if (deg % 2 == 1) {
            res *= base;
        }
        base *= base;
        deg /= 2;
    }
    return res;
}

BigInteger gcd(BigInteger a, BigInteger b) {
    a = abs(a);
    b = abs(b);
    while (b != 0) {
        a %= b;
        std::swap(a, b);
    }
    return a;
}

class Rational {
public:

    Rational() = default;

    Rational(const Rational& rat_n) = default;

    Rational(const BigInteger& big_n): nom(big_n) {}

    Rational(const int n): nom(n) {}

    Rational(const BigInteger& a, const BigInteger& b): nom(b > 0 ? a : -a), denom(abs(b)) {
        reduce();
    }

    Rational& operator=(Rational big_n) {
        swap(big_n);
        return *this;
    }

    explicit operator double() const;

    std::string asDecimal(size_t precision = 0) const;

    std::string toString() const {
        return denom == 1 ? nom.toString() : nom.toString() + '/' + denom.toString();
    }

    Rational operator-() const {
        return Rational(-nom, denom);
    }

    Rational& operator+=(const Rational& rat_n);

    Rational& operator-=(const Rational& rat_n);

    Rational& operator*=(const Rational& rat_n);

    Rational& operator/=(const Rational& rat_n) {
        return *this *= Rational(rat_n.denom, rat_n.nom);
    }

    // для уменьшения количества друзей
    const BigInteger& get_nom() {
        return nom;
    }

    const BigInteger& get_denom() {
        return denom;
    }

private:

    BigInteger nom = 1, denom = 1;

    void swap(Rational& big_n);

    void reduce();
};

Rational::operator double() const {
    BigInteger big_res = (nom * 1e16) / denom;
    double shift = 1e-16;
    double res = 0;
    if (big_res == 0) {
        return res;
    }
    while (shift <= 1e16 && big_res.mod10() == 0) {
        shift *= 10;
        big_res /= 10;
    }
    while (shift <= 1e16 && big_res != 0) {
        res += big_res.mod10() * shift;
        shift *= 10;
        big_res /= 10;
    }
    if (big_res != 0) {
        return UINTMAX_MAX;
    } else {
        return res;
    }
}

std::string Rational::asDecimal(size_t precision) const {
    std::string res = nom.get_inv() ? "-" : "";
    res += (nom / denom).toString();
    BigInteger shift = pow(10, precision);
    BigInteger fractional = abs(((nom * shift) / denom) % shift);
    res += '.';
    for (size_t i = 0; i < precision - fractional.count_digits(); ++i) {
        res += '0';
    }
    res.append(fractional.toString());
    return res;
}

Rational& Rational::operator+=(const Rational& rat_n) {
    nom = (nom * rat_n.denom) + (rat_n.nom * denom);
    denom *= rat_n.denom;
    reduce();
    return *this;
}

Rational& Rational::operator-=(const Rational& rat_n) {
    nom = (nom * rat_n.denom) - (rat_n.nom * denom);
    denom *= rat_n.denom;
    reduce();
    return *this;
}

Rational& Rational::operator*=(const Rational& rat_n) {
    nom *= rat_n.nom;
    denom *= rat_n.denom;
    reduce();
    return *this;
}

void Rational::swap(Rational& big_n) {
    std::swap(nom, big_n.nom);
    std::swap(denom, big_n.denom);
}

void Rational::reduce() {
    BigInteger common = gcd(nom, denom);
    nom /= common;
    denom /= common;
}

Rational operator+(const Rational& a, const Rational& b) {
    Rational res(a);
    res += b;
    return res;
}

Rational operator-(const Rational& a, const Rational& b) {
    Rational res(a);
    res -= b;
    return res;
}

Rational operator*(const Rational& a, const Rational& b) {
    Rational res(a);
    res *= b;
    return res;
}

Rational operator/(const Rational& a, const Rational& b) {
    Rational res(a);
    res /= b;
    return res;
}

bool operator<(const Rational& a, const Rational& b) {
    return (a - b).get_nom().get_inv();
}

bool operator>(const Rational& a, const Rational& b) {
    return b < a;
}

bool operator<=(const Rational& a, const Rational& b) {
    return !(a > b);
}

bool operator>=(const Rational& a, const Rational& b) {
    return !(a < b);
}

bool operator==(const Rational& a, const Rational& b) {
    return !(a < b || a > b);
}

bool operator!=(const Rational& a, const Rational& b) {
    return !(a == b);
}
