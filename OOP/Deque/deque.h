#include <iostream>
#include <memory>
#include <iterator>

template<typename T>
class Deque {
    class Pos;

    template<bool C>
    class common_iterator;

public:
    Deque();

    Deque(const Deque& other);

    explicit Deque(long long sz, const T& value = T());

    Deque& operator=(Deque other) {
        swap(other);
        return *this;
    }

    size_t size() const noexcept {
        return end_pos - begin_pos;
    }

    using iterator = common_iterator<false>;
    using const_iterator = common_iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    iterator begin() {
        return iterator(begin_pos, pool);
    }

    const_iterator begin() const {
        return cbegin();
    }

    const_iterator cbegin() const {
        return const_iterator(begin_pos, pool);
    }

    iterator end() {
        return iterator(end_pos, pool);
    }

    const_iterator end() const {
        return cend();
    }

    const_iterator cend() const {
        return const_iterator(end_pos, pool);
    }

    reverse_iterator rbegin() {
        return reverse_iterator(end() - 1);
    }

    const_reverse_iterator rbegin() const {
        return crbegin();
    }

    const_reverse_iterator crbegin() const {
        return const_reverse_iterator(cend() - 1);
    }

    reverse_iterator rend() {
        return reverse_iterator(begin() - 1);
    }

    const_reverse_iterator rend() const {
        return crend();
    }

    const_reverse_iterator crend() const {
        return const_reverse_iterator(cbegin() - 1);
    }

    T& operator[](long long i) {
        return *(begin() + i);
    }

    const T& operator[](long long i) const {
        return *(cbegin() + i);
    }

    const T& at(long long i) const;

    T& at(long long i) {
        return const_cast<T&>(static_cast<const Deque&>(*this).at(i));
    };

    void push_back(const T& value);

    void pop_back() noexcept {
        iterator back = end() - 1;
        back->~T();
        --end_pos;
    }

    void push_front(const T& value);

    void pop_front() noexcept {
        begin()->~T();
        ++begin_pos;
    }

    void insert(const iterator& it, const T& value);

    void erase(const iterator& it) noexcept;

private:
    long long bucket_size = 8;  // cannot make const because operator=
    long long buckets_reserved = 0;
    long long last_begin_gap = 0, last_end_gap = 0;
    Pos begin_pos, end_pos;
    T** pool = nullptr;

    void swap(Deque& other);

    T* make_bucket() {
        return reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
    }

    T** make_pool(long long buckets);

    void delete_pool();

    void resize_pool(long long old_size, long long new_size, long long new_begin);

    void fix_sizes();

    class Pos {
    public:
        long long x, y;

        Pos(): x(0), y(0), mod(1) {}

        Pos(long long x, long long y, long long mod): x(x), y(y), mod(mod) {
            normalise();
        }

        Pos(const Pos& other): x(other.x), y(other.y), mod(other.mod) {}

        Pos& operator=(Pos other) {
            swap(other);
            return *this;
        }

        Pos& operator++() {
            ++y;
            normalise();
            return *this;
        }

        Pos& operator--() {
            --y;
            normalise();
            return *this;
        }

        Pos& operator+=(long long num) {
            y += num;
            normalise();
            return *this;
        }

        Pos operator+(long long num) {
            Pos copy(x, y, mod);
            copy += num;
            return copy;
        }

        Pos& operator-=(long long num) {
            y -= num;
            normalise();
            return *this;
        }

        Pos operator-(long long num) const {
            Pos copy(x, y, mod);
            copy -= num;
            return copy;
        }

        long long operator-(const Pos& other) const {  // distance [*this, other)
            return (x * mod + y) - static_cast<long long>(other.x * other.mod + other.y);
        }

        bool operator<(const Pos& other) const {
            return *this - other < 0;
        }

        bool operator>(const Pos& other) const {
            return other < *this;
        }

        bool operator>=(const Pos& other) const {
            return !(*this < other);
        }

        bool operator<=(const Pos& other) const {
            return !(*this > other);
        }

        bool operator==(const Pos& other) const {
            return !(*this > other || other > *this);
        }

        bool operator!=(const Pos& other) const {
            return !(*this == other);
        }

    private:
        long long mod;  // cannot make const because operator=

        void normalise();

        void swap(Pos& other);
    };

    template<bool C>
    class common_iterator {
    public:
        using iterator_category = std::random_access_iterator_tag;
        using difference_type   = long long;
        using value_type        = std::conditional_t<C, const T, T>;
        using pointer           = std::conditional_t<C, const T*, T*>;
        using reference         = std::conditional_t<C, const T&, T&>;

        common_iterator(const Pos& pos, std::conditional_t<C, T** const&, T**&> pool): pos(pos), pool(pool),
                                                                                       value(&pool[pos.x][pos.y]) {}

        common_iterator(const common_iterator& other): common_iterator(other.pos, other.pool) {}

        template<bool U = C, std::enable_if_t<U, bool> = true>
        common_iterator(const iterator& iter): common_iterator(iter.pos, iter.pool) {}

        common_iterator& operator=(common_iterator other) {
            swap(other);
            return *this;
        }

        common_iterator& operator++() {
            ++pos;
            value = &pool[pos.x][pos.y];
            return *this;
        }

        common_iterator operator++(int) {
            common_iterator copy(*this);
            ++*this;
            return copy;
        }

        common_iterator& operator--() {
            --pos;
            value = &pool[pos.x][pos.y];
            return *this;
        }

        common_iterator operator--(int) {
            common_iterator copy(*this);
            --*this;
            return copy;
        }

        common_iterator& operator+=(int i) {
            pos += i;
            value = &pool[pos.x][pos.y];
            return *this;
        }

        common_iterator& operator-=(int i) {
            pos -= i;
            value = &pool[pos.x][pos.y];
            return *this;
        }

        common_iterator operator+(int i) const {
            common_iterator copy(*this);
            copy += i;
            return copy;
        }

        common_iterator operator-(int i) const {
            common_iterator copy(*this);
            copy -= i;
            return copy;
        }

        difference_type operator-(const common_iterator& other) const {
            return pos - other.pos;
        }

        reference operator*() const {
            return *value;
        }

        pointer operator->() const {
            return value;
        }

        bool operator<(const common_iterator& other) const {
            return pos < other.pos;
        }

        bool operator>(const common_iterator& other) const {
            return other < *this;
        }

        bool operator>=(const common_iterator& other) const {
            return !(*this < other);
        }

        bool operator<=(const common_iterator& other) const {
            return !(*this > other);
        }

        bool operator==(const common_iterator& other) const {
            return !(*this > other || other > *this);
        }

        bool operator!=(const common_iterator& other) const {
            return !(*this == other);
        }

    private:
        Pos pos;
        std::conditional_t<C, T** const&, T**&> pool;
        T* value;

        void swap(common_iterator& other) {
            std::swap(pos, other.pos);
            std::swap(pool, other.pool);
            std::swap(value, other.value);
        }
    };
};

template<typename T>
void Deque<T>::Pos::normalise() {
    x += y / mod;
    y %= mod;
    if (y < 0) {
        --x;
        y += mod;
    }
}

template<typename T>
void Deque<T>::Pos::swap(Deque::Pos& other) {
    std::swap(x, other.x);
    std::swap(y, other.y);
    std::swap(mod, other.mod);
}

template<typename T>
Deque<T>::Deque(): begin_pos(1, 0, bucket_size), end_pos(1, 0, bucket_size) {
    buckets_reserved = 2;
    last_begin_gap = 1;
    last_end_gap = 1;
    pool = make_pool(buckets_reserved);
}

template<typename T>
Deque<T>::Deque(const Deque& other): bucket_size(other.bucket_size), buckets_reserved(other.buckets_reserved),
                                     last_begin_gap(other.last_begin_gap), last_end_gap(other.last_end_gap),
                                     begin_pos(other.begin_pos), end_pos(other.end_pos) {
    pool = make_pool(buckets_reserved);
    try {
        std::uninitialized_copy(other.begin(), other.end(), begin());
    } catch (...) {
        delete_pool();
        throw;
    }
}

template<typename T>
Deque<T>::Deque(const long long sz, const T& value) {
    long long buckets_needed = (sz - 1) / bucket_size + 1;  // ceil(sz / bucket_size)
    long long reserve_gap = 1ll;
    buckets_reserved = 2 * reserve_gap + buckets_needed;
    pool = make_pool(buckets_reserved);
    begin_pos = {reserve_gap, 0, bucket_size};
    end_pos = {reserve_gap + buckets_needed - 1, sz % bucket_size, bucket_size};
    if (end_pos.y == 0) {
        ++end_pos.x;
    }
    std::uninitialized_fill(begin(), end(), value);
}

template<typename T>
void Deque<T>::swap(Deque& other) {
    std::swap(begin_pos, other.begin_pos);
    std::swap(end_pos, other.end_pos);
    std::swap(bucket_size, other.bucket_size);
    std::swap(buckets_reserved, other.buckets_reserved);
    std::swap(last_begin_gap, other.last_begin_gap);
    std::swap(last_end_gap, other.last_end_gap);
    std::swap(pool, other.pool);
}

template<typename T>
const T& Deque<T>::at(long long i) const {
    auto it = begin() + i;
    if (begin() <= it && it < end()) {
        return *it;
    } else {
        throw std::out_of_range("Deque index out of range: "
                                + std::to_string(i) + " not in [0, " + std::to_string(size()) + ")");
    }
}

template<typename T>
void Deque<T>::push_back(const T& value) {
    try {
        std::uninitialized_fill_n(end(), 1, value);
        ++end_pos;
        fix_sizes();
    } catch (...) {
        throw;
    }
}

template<typename T>
void Deque<T>::push_front(const T& value) {
    try {
        --begin_pos;
        fix_sizes();
        std::uninitialized_fill_n(begin(), 1, value);
    } catch (...) {
        ++begin_pos;
        throw;
    }
}

template<typename T>
void Deque<T>::insert(const Deque::iterator& it, const T& value) {
    try {
        T* to_next = new T(value);
        for (iterator cur_it = it; cur_it != end(); ++cur_it) {
            std::swap(*to_next, *cur_it);
        }
        std::uninitialized_fill_n(end(), 1, *to_next);
        ++end_pos;
        fix_sizes();
    } catch (...) {
        throw;
    }
}

template<typename T>
void Deque<T>::erase(const Deque::iterator& it) noexcept {
    T* to_prev = &*rbegin();
    for (iterator cur_it = end() - 2; cur_it >= it; --cur_it) {
        std::swap(*to_prev, *cur_it);
    }
    rbegin()->~T();
    --end_pos;
}

template<typename T>
T** Deque<T>::make_pool(long long int buckets) {
    T** res = new T*[buckets];
    for (int i = 0; i < buckets; ++i) {
        res[i] = make_bucket();
    }
    return res;
}

template<typename T>
void Deque<T>::delete_pool() {
    for (int i = 0; i < buckets_reserved; ++i) {
        delete[] pool[i];
    }
    delete[] pool;
}

template<typename T>
void Deque<T>::resize_pool(long long int old_size, long long int new_size, long long int new_begin) {
    T** new_pool = new T*[new_size];
    long long b = 0;

    while (b < new_begin) {
        new_pool[b] = make_bucket();
        ++b;
    }
    // new_begin <= b
    while (b < new_begin + old_size) {
        new_pool[b] = pool[b - new_begin];
        ++b;
    }
    // new_begin + old_size <= b
    while (b < new_size) {
        new_pool[b] = make_bucket();
        ++b;
    }

    delete[] pool;
    pool = new_pool;
    begin_pos += new_begin * bucket_size;
    end_pos += new_begin * bucket_size;
}

template<typename T>
void Deque<T>::fix_sizes() {
    if (begin_pos.x < 0) {
        last_begin_gap *= 2;
        buckets_reserved += last_begin_gap;
        resize_pool(buckets_reserved - last_begin_gap, buckets_reserved, last_begin_gap);
    }
    if (end_pos.x >= buckets_reserved) {
        last_end_gap *= 2;
        buckets_reserved += last_end_gap;
        resize_pool(buckets_reserved - last_end_gap, buckets_reserved, 0);
    }
}
