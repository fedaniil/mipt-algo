#include <utility>
#include <cstddef>
#include <iostream>
#include <vector>
#include <cmath>

namespace my_traits {
    template<typename T, typename ...Allowed>
    struct restrict_type: std::conditional_t<(std::is_same_v<T, Allowed> || ...), std::true_type, std::false_type> {};

    template<typename T, typename ...Allowed>
    inline constexpr bool restrict_type_v = restrict_type<T, Allowed...>::value;

    template<typename U, typename T, typename ...Allowed>
    struct enable_when_restricted: std::enable_if<restrict_type_v<T, Allowed...>, U> {};

    template<typename U, typename T, typename ...Allowed>
    using enable_when_restricted_t = typename enable_when_restricted<U, T, Allowed...>::type;

    template<typename T>
    struct remove_cvref {
        typedef std::remove_cv_t<std::remove_reference_t<T>> type;
    };

    template<typename T>
    using remove_cvref_t = typename remove_cvref<T>::type;
}

template<typename Key, typename Value,
        typename Hash = std::hash<Key>,
        typename Equal = std::equal_to<Key>,
        typename Alloc = std::allocator<std::pair<const Key, Value>>>
class UnorderedMap {
public:
    using NodeType = std::pair<const Key, Value>;

private:
    struct ListNode;

    template<bool C, typename ValueType = std::conditional_t<C, const NodeType, NodeType>,
            typename PointerType = std::conditional_t<C, const NodeType*, NodeType*>,
            typename ReferenceType = std::conditional_t<C, const NodeType&, NodeType&>>
    class common_iterator;

    Hash hasher;
    Equal key_equal;

    Alloc value_alloc;
    using AllocTraits = std::allocator_traits<Alloc>;

    typename AllocTraits::template rebind_alloc<ListNode> node_alloc;
    using NodeAllocTraits = std::allocator_traits<decltype(node_alloc)>;

public:

    template<typename AllocT = Alloc, typename = my_traits::enable_when_restricted_t<void, AllocT, Alloc>>
    explicit UnorderedMap(size_t bucket_count = 1, const Hash& hash = Hash(),
                          const Equal& equal = Equal(), AllocT&& alloc = Alloc()): hasher(hash), key_equal(equal),
                          value_alloc(std::forward<AllocT>(alloc)), buckets(bucket_count, Iterator(fictive_begin)),
                          next(bucket_count, std::numeric_limits<size_t>::max()) {
        init_fictive_nodes();
    }

    UnorderedMap(const UnorderedMap& other): UnorderedMap(other, AllocTraits::select_on_container_copy_construction(other.value_alloc)) {}

    template<typename AllocT = Alloc, typename = my_traits::enable_when_restricted_t<void, AllocT, Alloc>>
    UnorderedMap(const UnorderedMap& other, AllocT&& alloc): hasher(other.hasher), key_equal(other.key_equal),
                                                             value_alloc(std::forward<AllocT>(alloc)) {
        init_fictive_nodes();
        insert(other.begin(), other.end());
    }

    UnorderedMap(UnorderedMap&& other): UnorderedMap(std::move(other), std::move(other.value_alloc)) {}

    UnorderedMap(UnorderedMap&& other, Alloc&& alloc): hasher(std::move(other.hasher)), key_equal(std::move(other.key_equal)),
                                                       value_alloc(std::move(alloc)), node_alloc(std::move(other.node_alloc)),
                                                       fictive_begin(other.fictive_begin), fictive_end(other.fictive_end),
                                                       sz(other.sz), max_factor(other.max_factor),
                                                       buckets(std::move(other.buckets)), next(std::move(other.next)),
                                                       bucket_starting_on_fictive(other.bucket_starting_on_fictive) {
        other.after_move();
    }

    UnorderedMap& operator=(const UnorderedMap& other) {
        UnorderedMap copy(other, AllocTraits::propagate_on_container_copy_assignment::value ? other.value_alloc : std::move(value_alloc));
        swap(copy);
        return *this;
    }

    template<typename KeyT = Key, typename = my_traits::enable_when_restricted_t<void, KeyT, Key>>
    UnorderedMap& operator=(UnorderedMap&& other) noexcept;

    Alloc get_allocator() const {
        return value_alloc;
    }

    size_t size() const noexcept {
        return sz;
    }

    size_t bucket_count() const noexcept {
        return buckets.size();
    }

    float load_factor() const noexcept {
        return static_cast<float>(size()) / bucket_count();
    }

    float max_load_factor() const noexcept {
        return max_factor;
    }

    void max_load_factor(float new_max_factor) {
        max_factor = new_max_factor;
        rehash(buckets.size());
    }

    void reserve(size_t element_count) {
        rehash(std::ceil(element_count / max_load_factor()));
    }

    void rehash(size_t bucket_count);

    using Iterator = common_iterator<false>;
    using ConstIterator = common_iterator<true>;
    using MoveIterator = common_iterator<false, NodeType, NodeType*, NodeType&&>;

    Iterator begin() {
        return Iterator(fictive_begin->next);
    }

    ConstIterator begin() const {
        return cbegin();
    }

    ConstIterator cbegin() const {
        return ConstIterator(fictive_begin->next);
    }

    MoveIterator mbegin() {
        return MoveIterator(fictive_begin->next);
    }

    Iterator end() {
        return Iterator(fictive_end);
    }

    ConstIterator end() const {
        return cend();
    }

    ConstIterator cend() const {
        return ConstIterator(fictive_end);
    }

    MoveIterator mend() {
        return MoveIterator(fictive_end);
    }

    template<typename KeyT = Key, typename = my_traits::enable_when_restricted<void, KeyT, Key>>
    Value& operator[](KeyT&& key);

    Value& at(const Key& key) {
        return const_cast<Value&>(static_cast<const UnorderedMap&>(*this).at(key));
    }

    const Value& at(const Key& key) const;

    template<typename NodeTypeT = NodeType>
    auto insert(NodeTypeT&& node) {
        return emplace(std::forward<NodeTypeT>(node));
    }

    template<typename InputIterator>
    auto insert(InputIterator from, const InputIterator& to)
            -> my_traits::enable_when_restricted_t<void,
            typename my_traits::remove_cvref_t<typename std::iterator_traits<InputIterator>::value_type>, NodeType> {
        for (; from != to; ++from) {
            insert(*from);
        }
    }

    template<typename ...Args>
    auto emplace(Args&&... args) {
        return insert_list_node(make_node(value_alloc, hasher, std::forward<Args>(args)...));
    }

    auto emplace(Key&& key, Value&& value) { // for 2 initializer lists
        return emplace<>(std::move(key), std::move(value));
    }

    auto emplace(NodeType&& node_type_value) { // for initializer list
        return emplace<>(std::move(node_type_value));
    }

    void erase(const ConstIterator& iterator) {
        utilize_node(extract_list_node(iterator.list_node, buckets, bucket_count(), next,
                                            fictive_begin, fictive_end));
        --sz;
    }

    void erase(ConstIterator from, const ConstIterator& to) {
        while (from != to) {
            erase(from++);
        }
    }

    Iterator find(const Key& key) {
        return Iterator(static_cast<const UnorderedMap&>(*this).find(key)); // using private constructor
    }

    ConstIterator find(const Key& key) const {
        auto hash = hasher(key);
        auto bucket = hash % bucket_count();
        for (auto node = buckets[bucket].list_node->next; in_bucket(*node, bucket); node = node->next) {
            if (key_equal(node->value.first, key)) {
                return ConstIterator(node);
            }
        }
        return cend();
    }

    void clear() {
        erase(begin(), end());
    }

    ~UnorderedMap() {
        clear();
        NodeAllocTraits::deallocate(node_alloc, fictive_end, 1);
        NodeAllocTraits::deallocate(node_alloc, fictive_begin, 1);
    }

private:
    ListNode* fictive_begin = make_base_node();
    ListNode* fictive_end = make_base_node();
    size_t sz = 0;
    float max_factor = 1.0;
    inline static size_t expand_factor = 4;

    std::vector<Iterator> buckets = {Iterator(fictive_begin)};
    std::vector<size_t> next = {std::numeric_limits<size_t>::max()};
    size_t bucket_starting_on_fictive = std::numeric_limits<size_t>::max();

    struct ListNode {
        ListNode* next = nullptr;
        ListNode* prev = nullptr;
        Alloc& value_alloc;
        NodeType value;
        size_t hash;

        template<typename ...Args>
        ListNode(Args&&... any_args) = delete; // you must use static ::construct

        template<typename ...Args>
        static void construct(ListNode* node, Alloc& alloc, const Hash& hasher, Args&&... value_args) {
            node->prev = node->next = nullptr;
            node->value_alloc = alloc;
            AllocTraits::construct(node->value_alloc, &(node->value), std::forward<Args>(value_args)...);
            node->hash = hasher(node->value.first);
        }

        ~ListNode() = delete; // you must use static ::destroy

        static void destroy(ListNode* node) {
            AllocTraits::destroy(node->value_alloc, &node->value);
        }
    };

    template<bool C, typename ValueType, typename PointerType, typename ReferenceType>
    class common_iterator {
    public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = ValueType;
        using pointer           = PointerType;
        using reference         = ReferenceType;

        common_iterator(ListNode* x): list_node(reinterpret_cast<ListNode*>(x)) {}

        common_iterator(const common_iterator& other): list_node(other.list_node) {}

        template<bool U = C, typename = std::enable_if_t<U, void>>
        common_iterator(const Iterator& iter): list_node(iter.list_node) {}

        common_iterator& operator=(common_iterator other) {
            swap(other);
            return *this;
        }

        common_iterator& operator++() {
            list_node = list_node->next;
            return *this;
        }

        common_iterator operator++(int) {
            common_iterator copy(*this);
            ++*this;
            return copy;
        }

        common_iterator& operator--() {
            list_node = list_node->prev;
            return *this;
        }

        common_iterator operator--(int) {
            common_iterator copy(*this);
            --*this;
            return copy;
        }

        reference operator*() const {
            if constexpr (std::is_rvalue_reference_v<reference>) {
                return std::move(list_node->value);
            } else {
                return list_node->value;
            }
        }

        pointer operator->() const {
            return &list_node->value;
        }

        bool operator==(const common_iterator& other) const {
            return list_node == other.list_node;
        }

        bool operator!=(const common_iterator& other) const {
            return !(*this == other);
        }

        friend UnorderedMap;

    private:
        ListNode* list_node;

        void swap(common_iterator& other) {
            std::swap(list_node, other.list_node);
        }

        template<bool U = C, typename = std::enable_if_t<!U, void>>
        explicit common_iterator(const ConstIterator& iter): list_node(iter.list_node) {}
    };

    void swap(UnorderedMap& other);

    void check_rehash() {
        if (load_factor() > max_load_factor()) {
            rehash(std::max(static_cast<size_t>(2), expand_factor) * bucket_count());
        }
    }

    ListNode* make_base_node() {
        auto ptr = NodeAllocTraits::allocate(node_alloc, 1);
        ptr->next = nullptr;
        ptr->prev = nullptr;
        return ptr;
    }

    template<typename ...Args>
    ListNode* make_node(Args&&... args) {
        auto ptr = NodeAllocTraits::allocate(node_alloc, 1);
        ListNode::construct(ptr, std::forward<Args>(args)...);
        return ptr;
    }

    void utilize_node(ListNode* node) {
        ListNode::destroy(node);
        NodeAllocTraits::deallocate(node_alloc, node, 1);
    }

    void init_fictive_nodes() {
        init_fictive_nodes(fictive_begin, fictive_end);
    };

    void init_fictive_nodes(ListNode* beg, ListNode* end) {
        beg->next = end;
        end->prev = beg;
        beg->hash = end->hash = std::numeric_limits<size_t>::max();
    };

    void insert_after(ListNode* left_node, ListNode* new_node) {
        insert_between(left_node, left_node->next, new_node);
    }

    void insert_between(ListNode* left_node, ListNode* right_node, ListNode* new_node) {
        left_node->next = new_node;
        right_node->prev = new_node;
        new_node->next = right_node;
        new_node->prev = left_node;
    }

    template<typename ...Args>
    ListNode* extract_list_node(ListNode* node, Args&&... args_for_prepare);

    void after_move() {
        fictive_begin = make_base_node();
        fictive_end = make_base_node();
        init_fictive_nodes();
        sz = 0;
    }

    bool in_bucket(const ConstIterator& it, size_t bucket) const {
        return in_bucket(it.list_node, bucket);
    }

    bool in_bucket(const ListNode& node, size_t bucket) const {
        return (node.hash % bucket_count() == bucket) && (&node != fictive_end);
    }

    void prepare_to_extract(ListNode* node, decltype(buckets)& buckets_in_use, size_t bucket_count,
                            decltype(next)& next_in_use, ListNode* fictive_begin_in_use, ListNode* fictive_end_in_use);

    void fix_first_bucket(const Iterator& new_begin, size_t new_first_bucket);

    std::pair<Iterator, bool> insert_list_node(ListNode* node);
};

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::swap(UnorderedMap& other) {
    std::swap(hasher, other.hasher);
    std::swap(key_equal, other.key_equal);
    std::swap(value_alloc, other.value_alloc);
    std::swap(fictive_begin, other.fictive_begin);
    std::swap(fictive_end, other.fictive_end);
    std::swap(sz, other.sz);
    std::swap(max_factor, other.max_factor);
    std::swap(buckets, other.buckets);
    std::swap(next, other.next);
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template<typename KeyT, typename>
UnorderedMap<Key, Value, Hash, Equal, Alloc>& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator=(UnorderedMap&& other) noexcept {
    clear();
    hasher = std::move(other.hasher);
    key_equal = std::move(other.key_equal);
    max_load_factor(other.max_load_factor());
    auto common_move = [this, &other]() {
        fictive_begin = other.fictive_begin;
        fictive_end = other.fictive_end;
        sz = other.sz;
        buckets = std::move(other.buckets);
        next = std::move(other.next);
        bucket_starting_on_fictive = other.bucket_starting_on_fictive;
    };
    if constexpr (AllocTraits::propagate_on_container_move_assignment::value) {
        value_alloc = std::move(other.value_alloc);
        node_alloc = std::move(other.node_alloc);
        common_move();
    } else {
        if (value_alloc == other.value_alloc) {
            common_move();
        } else {
            if constexpr (std::is_copy_constructible_v<KeyT>) {
                reserve(other.buckets.size());
                insert(other.mbegin(), other.mend());
            } else {
                throw std::invalid_argument("Key needs to be copy-constructible when move-assigning UnorderedMaps "
                                            "with different allocators and propagate_on_container_move_assignment "
                                            "== false");
            }
        }
    }
    other.after_move();
    return *this;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::rehash(size_t bucket_count) {
    auto new_bucket_count = std::max(bucket_count, static_cast<size_t>(std::ceil(size() / max_load_factor())));
    if (buckets.size() != new_bucket_count) {
        auto old_fictive_begin = fictive_begin;
        auto old_fictive_end = fictive_end;
        fictive_begin = make_base_node();
        fictive_end = make_base_node();
        init_fictive_nodes();
        sz = 0;
        decltype(buckets) old_buckets = std::move(buckets);
        decltype(next) old_next = std::move(next);
        buckets = decltype(buckets)(new_bucket_count, Iterator(fictive_begin));
        next = decltype(next)(new_bucket_count, std::numeric_limits<size_t>::max());
        for (auto first = old_fictive_begin->next; first != old_fictive_end; first = old_fictive_begin->next) {
            auto list_node = extract_list_node(first, old_buckets, old_buckets.size(), old_next, old_fictive_begin, old_fictive_end);
            insert_list_node(list_node);
        }
        NodeAllocTraits::deallocate(node_alloc, old_fictive_begin, 1);
        NodeAllocTraits::deallocate(node_alloc, old_fictive_end, 1);
    }
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template<typename KeyT, typename>
Value& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator[](KeyT&& key) {
    auto possible_it = find(key);
    if (possible_it == end()) {
        if constexpr (std::is_default_constructible_v<Value>) {
            return emplace(std::forward<KeyT>(key), Value()).first->second;
        } else {
            throw std::out_of_range("Key is not found and Value cannot be default-constructed");
        }
    }
    return possible_it->second;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
const Value& UnorderedMap<Key, Value, Hash, Equal, Alloc>::at(const Key& key) const {
    auto possible_it = find(key);
    if (possible_it == end()) {
        throw std::out_of_range("Key is not found");
    }
    return possible_it->second;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template<typename... Args>
auto UnorderedMap<Key, Value, Hash, Equal, Alloc>::extract_list_node(UnorderedMap::ListNode* node,
                                                                     Args&& ... args_for_prepare)
                                                                     -> UnorderedMap::ListNode* {
    prepare_to_extract(node, std::forward<Args>(args_for_prepare)...);
    auto prev_node = node->prev;
    auto next_node = node->next;
    prev_node->next = next_node;
    next_node->prev = prev_node;
    node->next = nullptr;
    node->prev = nullptr;
    return node;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::prepare_to_extract(UnorderedMap::ListNode* node,
                                                                      decltype(buckets)& buckets_in_use,
                                                                      size_t bucket_count,
                                                                      decltype(next)& next_in_use,
                                                                      UnorderedMap::ListNode* fictive_begin_in_use,
                                                                      UnorderedMap::ListNode* fictive_end_in_use) {
    auto hash = node->hash;
    auto bucket = hash % bucket_count;
    // Check if the node is the last in bucket
    if (next_in_use[bucket] < buckets_in_use.size() && buckets_in_use[next_in_use[bucket]].list_node == node) {
        --buckets_in_use[next_in_use[bucket]];
    }
    // Check if the node is the only in bucket: middle bucket case and last bucket case
    if ((next_in_use[bucket] < buckets_in_use.size() && buckets_in_use[next_in_use[bucket]] == buckets_in_use[bucket])
        || (buckets_in_use[bucket].list_node->next == node && node->next == fictive_end_in_use)) {
        buckets_in_use[bucket] = fictive_begin_in_use;
    }
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::fix_first_bucket(const UnorderedMap::Iterator& new_begin,
                                                                    size_t new_first_bucket) {
    if (bucket_starting_on_fictive < bucket_count()) {
        buckets[bucket_starting_on_fictive] = new_begin;
        next[new_first_bucket] = bucket_starting_on_fictive;
    }
    bucket_starting_on_fictive = new_first_bucket;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
auto UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert_list_node(UnorderedMap::ListNode* node)
        -> std::pair<Iterator, bool> {
    auto element_it = find(node->value.first);
    bool found = true;
    if (element_it == end()) {
        found = false;
        auto bucket = node->hash % bucket_count();
        insert_after(buckets[bucket].list_node, node);
        element_it = Iterator(buckets[bucket].list_node->next);
        if (buckets[bucket].list_node == fictive_begin && bucket_starting_on_fictive != bucket) {
            fix_first_bucket(element_it, bucket);
        }
        ++sz;
        check_rehash();
    }
    return {element_it, !found};
}
