#include <iostream>
#include <cstring>

class String {
public:

    String(const char* c_str): sz(std::strlen(c_str)), buf_size(std::strlen(c_str)), str(new char[buf_size]) {
        std::copy(c_str, c_str + sz, str);
    }

    String(size_t n, char c): sz(n), buf_size(n), str(new char[buf_size]) {
        std::fill_n(str, n, c);
    }

    String(char c): String(1, c) {};

    String() = default;

    String(const String& s): sz(s.sz), buf_size(s.buf_size), str(new char[buf_size]) {
        std::copy(s.str, s.str + sz, str);
    }

    String& operator=(String s) {
        swap(s);
        return *this;
    }

    size_t length() const {
        return sz;
    }

    bool empty() const {
        return sz == 0;
    }

    char& front() {
        return *str;
    }

    const char& front() const {
        return *str;
    }

    char& back() {
        if (sz == 0) return *str;
        return *(str + (sz - 1));
    }

    const char& back() const {
        if (sz == 0) return *str;
        return *(str + (sz - 1));
    }

    char& operator[](size_t index) {
        return str[index];
    }

    const char& operator[](size_t index) const {
        return str[index];
    }

    void push_back(char c);

    char pop_back();

    String& operator+=(const String& s);

    String substr(size_t start, size_t count) const;

    size_t find(const String& s) const;

    size_t rfind(const String& s) const;

    void clear() {
        sz = 0;
        str_realloc(0);
    }

    ~String() {
        delete[] str;
    }

private:

    size_t sz = 0;
    size_t buf_size = 0;
    char* str = nullptr;

    void swap(String& s);

    void str_realloc(size_t new_sz);

    void str_extend() {
        str_realloc(std::max(static_cast<size_t>(1), buf_size * 2));
    }

    void str_shrink() {
        str_realloc(buf_size / 2);
    }
};

bool operator<(const String& a, const String& b) {
    if (a.length() != b.length()) {
        return a.length() < b.length();
    }
    for (size_t i = 0; i < a.length(); ++i) {
        if (a[i] != b[i]) {
            return a[i] < b[i];
        }
    }
    return false;
}

inline bool operator>(const String& a, const String& b) {
    return b < a;
}

inline bool operator<=(const String& a, const String& b) {
    return !(a > b);
}

inline bool operator>=(const String& a, const String& b) {
    return !(a < b);
}

inline bool operator==(const String& a, const String& b) {
    return !(a < b || b < a);
}

inline bool operator!=(const String& a, const String& b) {
    return !(a == b);
}

void String::push_back(char c) {
    if (sz + 1 > buf_size) {
        str_extend();
    }
    str[sz] = c;
    ++sz;
}

char String::pop_back() {
    --sz;
    char back = str[sz];
    if (sz <= buf_size / 2) {
        str_shrink();
    }
    return back;
}

String& String::operator+=(const String& s) {
    if (buf_size < sz + s.sz) {
        str_realloc(std::max(static_cast<size_t>(1), 2 * (sz + s.sz)));
    }
    std::copy(s.str, s.str + s.sz, str + sz);
    sz += s.sz;
    return *this;
}

String String::substr(size_t start, size_t count) const {
    char* c_substr = new char[count];
    std::copy(str + start, str + (start + count), c_substr);
    return c_substr;
}

size_t String::find(const String& s) const {
    for (size_t i = 0; i + s.sz <= sz; ++i) {
        if (substr(i, s.sz) == s)
            return i;
    }
    return sz;
}

size_t String::rfind(const String& s) const {
    size_t ans = sz;
    for (size_t i = 0; i + s.sz <= sz; ++i) {
        String sub = substr(i, s.sz);
        if (s == sub) {
            ans = i;
        }
    }
    return ans;
}

void String::swap(String& s) {
    std::swap(sz, s.sz);
    std::swap(buf_size, s.buf_size);
    std::swap(str, s.str);
}

void String::str_realloc(size_t new_sz) {
    buf_size = new_sz;
    char* new_str = new char[buf_size];
    std::copy(str, str + sz, new_str);
    delete[] str;
    str = new_str;
}

String operator+(const String& a, const String& b) {
    String copy = a;
    copy += b;
    return copy;
}

std::ostream& operator<<(std::ostream& out, const String& s) {
    for (size_t i = 0; i < s.length(); ++i) {
        out << s[i];
    }
    return out;
}

std::istream& operator>>(std::istream& in, String& s) {
    s.clear();
    char c = in.get();
    while (isspace(c)) {
        c = in.get();
    }
    do {
        s += c;
        c = in.get();
    } while (!in.eof() && !isspace(c));

    return in;
}
