/* Задача E. Множества

На вступительном контесте в пилотную группу по программированию Вашему другу предложили реализовать
структуру данных для хранения множеств чисел. Так как он специализируется на истории литературы, данную
структуру придётся реализовать Вам.

Структура должна хранить m+1 множеств чисел от 0 до n, пронумерованных от 0 до m включительно, при этом
одно число может принадлежать сразу нескольким множествам. Изначально все множества пустые.

Вы должны реализовать следующие операции на этой структуре:
    ADD e s
- Добавить в множество №s (0≤s≤m) число e (0≤e≤n)
    DELETE e s
- Удалить из множества №s (0≤s≤m) число e (0≤e≤n). Гарантируется, что до этого число e было помещено в множество
    CLEAR s
- Очистить множество №s (0≤s≤m)
    LISTSET s
- Показать содержимое множества №s (0≤s≤m) в возрастающем порядке, либо −1, если множество пусто
    LISTSETSOF e
- Показать множества, в которых лежит число e (0≤e≤n), либо −1, если этого числа нет ни в одном множестве.

* Входные данные

Сначала вводятся числа N(1≤N≤9223372036854775807), M(1≤M≤100000) и K(0≤K≤100000) — максимальное число, номер
максимального множества и количество запросов к структуре данных. Далее следуют K строк указанного формата запросов.

* Выходные данные

На каждый запрос LISTSET Ваша программа должна вывести числа  — содержимое запрошенного множества или −1, если
множество пусто.

На каждый запрос LISTSETSOF Ваша программа должна вывести числа  — номера множеств, содержащих запрошенное число,
или −1, если таких множеств не существует.

На прочие запросы не должно быть никакого вывода.

Гарантируется, что правильный вывод программы не превышает одного мегабайта.
*/

#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>

struct compare_as_num {
    bool operator()(const std::string& a, const std::string& b) const {
        if (a.size() == b.size()) {
            for (int i = 0; i < a.size(); ++i) {
                if (a[i] != b[i]) {
                    return a[i] < b[i];
                }
            }
        }
        return a.size() < b.size();
    }
};

class Sets {
    std::vector<std::set<std::string, compare_as_num>> lst_of_sets;
    std::unordered_map<std::string, std::set<int>> num_to_entries;

public:

    Sets(int m): lst_of_sets(m + 1) {}

    void add(const std::string& e, int s) {
        lst_of_sets[s].emplace(e);
        num_to_entries[e].emplace(s);
    }

    void erase(const std::string& e, int s) {
        lst_of_sets[s].erase(e);
        num_to_entries[e].erase(s);
    };

    void clear(int s) {
        while (!lst_of_sets[s].empty()) {
            std::string fir_e = *(--lst_of_sets[s].end());
            num_to_entries[fir_e].erase(s);
            lst_of_sets[s].erase(--lst_of_sets[s].end());
        }
    }

    const std::set<std::string, compare_as_num>& get_set(int s) {
        return lst_of_sets[s];
    }

    const std::set<int>& get_num_entries(const std::string& e) {
        return num_to_entries[e];
    }
};

void exec(const std::string& cmd, Sets& sets_struct) {
    using std::cin, std::cout, std::string;

    string e;
    int s;
    if (cmd == "ADD") {
        cin >> e >> s;
        sets_struct.add(e, s);
    } else if (cmd == "DELETE") {
        cin >> e >> s;
        sets_struct.erase(e, s);
    } else if (cmd == "CLEAR") {
        cin >> s;
        sets_struct.clear(s);
    } else if (cmd == "LISTSET") {
        cin >> s;
        for (const string& num : sets_struct.get_set(s)) {
            cout << num << ' ';
        }
        if (sets_struct.get_set(s).empty()) {
            cout << "-1";
        }
        cout << '\n';

    } else if (cmd == "LISTSETSOF") {
        cin >> e;
        for (const int& entry : sets_struct.get_num_entries(e)) {
            cout << entry << ' ';
        }
        if (sets_struct.get_num_entries(e).empty()) {
            cout << "-1";
        }
        cout << '\n';
    }
}

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin;
    enable_fast_io();

    std::string n;
    int m, k;
    cin >> n >> m >> k;

    std::string cmd;
    Sets sets_struct(m);
    while (k--) {
        cin >> cmd;
        exec(cmd, sets_struct);
    }

    return 0;
}