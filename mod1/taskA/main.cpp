/* Задача A

Реализуйте свой стек. Решения, использующие std::stack, получат 1 балл. Решения, хранящие
стек в массиве, получат 1.5 балла. Решения, использующие указатели, получат 2 балла.

Гарантируется, что количество элементов в стеке ни в какой момент времени не превышает 10000.

Обработайте следующие запросы:

    push n: добавить число n в конец стека и вывести «ok»;
    pop: удалить из стека последний элемент и вывести его значение, либо вывести «error»,
         если стек был пуст;
    back: сообщить значение последнего элемента стека, либо вывести «error», если стек пуст;
    size: вывести количество элементов в стеке;
    clear: опустошить стек и вывести «ok»;
    exit: вывести «bye» и завершить работу.

* Входные данные

В каждой строке входных данных задана одна операция над стеком в формате, описанном выше.

* Выходные данные

В ответ на каждую операцию выведите соответствующее сообщение.
*/

#include <iostream>
#include <cassert>

struct Node {
    int value;
    Node* next;
};

struct Stack {
    Node* last = nullptr;
    int size = 0;

    bool is_empty() {
        return last == nullptr;
    }

    void push(int val) {
        Node* new_node = new Node();
        new_node->value = val;
        new_node->next = last;
        last = new_node;
        ++size;
    }

    int back() {
        assert(!is_empty());
        return last->value;
    }

    int pop() {
        assert(!is_empty());
        int last_value = last->value;
        Node* cur_last = last;
        last = last->next;
        free(cur_last);
        --size;
        return last_value;
    }

    void clear() {
        Node* cur = last;
        while (cur != nullptr) {
            Node* nxt = cur->next;
            delete cur;
            cur = nxt;
        }
        last = nullptr;
        size = 0;
    }
};

// выполняет команду cmd и возвращает false если команда на завершение работы, иначе true
bool execute(const std::string &cmd, Stack &stack) {
    using std::cin, std::cout, std::endl;

    if (cmd == "exit") {
        cout << "bye" << endl;
        return false;

    } else if (cmd == "push") {
        int n;
        cin >> n;
        stack.push(n);
        cout << "ok" << endl;

    } else if (cmd == "pop") {
        if (stack.is_empty()) {
            cout << "error" << endl;
        } else {
            cout << stack.pop() << endl;
        }
    } else if (cmd == "back") {
        if (stack.is_empty()) {
            cout << "error" << endl;
        } else {
            cout << stack.back() << endl;
        }
    } else if (cmd == "size") {
        cout << stack.size << endl;

    } else if (cmd == "clear") {
        stack.clear();
        cout << "ok" << endl;
    }

    return true;
}

int main() {
    using std::cin;

    Stack stack;
    std::string cmd;
    bool work = true;
    while (work) {
        cin >> cmd;
        work = execute(cmd, stack);
    }
    stack.clear();

    return 0;
}