/* Задача C. Гистограмма
Гистограмма является многоугольником, сформированным из последовательности прямоугольников,
выровненных на общей базовой линии. Прямоугольники имеют равную ширину, но могут иметь
различные высоты. Обычно гистограммы используются для представления дискретных распределений,
например, частоты символов в текстах. Отметьте, что порядок прямоугольников очень важен.
Вычислите область самого большого прямоугольника в гистограмме, который также находится на
общей базовой линии.

* Входные данные

В первой строке входного файла записано число N (0<N≤1e6) − количество прямоугольников
гистограммы. Затем следует N целых чисел h_1…h_n, где 0≤h_i≤1e9. Эти числа обозначают высоты
прямоугольников гистограммы слева направо. Ширина каждого прямоугольника равна 1.

* Выходные данные

Выведите площадь самого большого прямоугольника в гистограмме. Помните, что этот прямоугольник
должен быть на общей базовой линии.
*/

#include <iostream>
#include <stack>
#include <cassert>

typedef long long ll;
typedef std::pair<ll, ll> pairll;

struct Node {
    pairll value;
    Node* next;
};

// стек из задачи A, но с другим типом value в ноде (пара ll)
struct Stack {
    Node* last = nullptr;
    int size = 0;

    bool is_empty() {
        return last == nullptr;
    }

    void push(pairll val) {
        Node* new_node = new Node();
        new_node->value = val;
        new_node->next = last;
        last = new_node;
        ++size;
    }

    pairll back() {
        assert(!is_empty());
        return last->value;
    }

    pairll pop() {
        assert(!is_empty());
        pairll last_value = last->value;
        Node* cur_last = last;
        last = last->next;
        free(cur_last);
        --size;
        return last_value;
    }

    void clear() {
        Node* cur = last;
        while (cur != nullptr) {
            Node* nxt = cur->next;
            delete cur;
            cur = nxt;
        }
        last = nullptr;
        size = 0;
    }
};

ll find_max_square(int n) {
    using std::cin;

    Stack prev;
    ll x, ans = 0;
    for (int i = 0; i < n; ++i) {
        cin >> x;

        int pos = i;
        while (!prev.is_empty() && prev.back().first >= x) {
            ans = std::max(ans, (i - prev.back().second) * prev.back().first);
            // столбец i в "длину" не входит, т.к. его высота <= чем у последнего в очереди
            // (если равна, это будет учтено или в следующих i, или в последнем цикле)
            pos = prev.back().second;
            prev.pop();
        }
        prev.push({x, pos});
    }

    while (!prev.is_empty()) {
        ans = std::max(ans, (n - prev.back().second) * prev.back().first);
        // (n - i) - это кол-во элементов от i до последнего в 0-нумерации
        prev.pop();
    }

    return ans;
}

void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout, std::endl;
    enable_fast_io();

    int n;
    cin >> n;

    cout << find_max_square(n) << endl;

    return 0;
}
