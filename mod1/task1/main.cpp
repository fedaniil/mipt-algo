#include <iostream>

using namespace std;

/**
 * Принимает int n <= 1e5
 * Выводит в stdout через пробел все простые числа от 1 до n
 */
void print_primes(int n) {
    if (n == 1) {
        cout << endl;
        return;  // 1 - не простое число; пустой вывод
    }

    bool is_prime[n + 1];  // используем 1-нумерацию
    fill_n(is_prime, n + 1, true);

    for (int i = 2; i <= n; ++i) {
        if (is_prime[i]) {
            cout << i << ' ';
            for (int j = 2; i * j <= n; ++j) {  // перебираем числа, кратные i, начиная с 2i до n
                is_prime[i * j] = false;
            }
        }
    }

    cout << endl;
}

/**
 * Запрашивает из stdin число N <= 1e5
 * Выводит в stdout через пробел все простые числа от 1 до N включительно
 */
int main() {
    int n;
    cin >> n;

    print_primes(n);
    
    return 0;
}
