/* Задача E. Yet another data structure

Нужно отвечать на запросы вида:
    + x
– добавить в мультимножество число x
    ? x
– посчитать сумму чисел не больших x

* Формат входных данных

В первой строке содержится число запросов 1<=q<=1e5. Далее каждая строка содержит один запрос.
Все числа x целые от 0 до 1e9−1.

* Формат выходных данных

Ответы на все запросы вида “? x”.
*/

#include <iostream>
#include <vector>
#include <algorithm>

class Fenwick_tree {
    size_t sz = 0;
    std::vector<long long> pool;

    long long sum(int i) {
        long long res = 0;
        while (i >= 0) {
            res += pool[i];
            i &= (i + 1);
            --i;
        }
        return res;
    }

public:

    Fenwick_tree(size_t n): sz(n), pool(n) {}

    void add(size_t i, long long d) {
        while (i < sz) {
            pool[i] += d;
            i |= (i + 1);
        }
    }

    long long seg_sum(int i, int j) {
        return sum(j) - sum(i - 1);
    }
};

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int q;
    cin >> q;
    char type;
    int x;
    std::vector<std::pair<char, int>> actions;
    std::vector<std::pair<int, int>> used_nums;
    for (int i = 0; i < q; ++i) {
        cin >> type >> x;
        actions.emplace_back(type, x);
        used_nums.emplace_back(x, i);
    }

    // сжатие координат
    std::sort(used_nums.begin(), used_nums.end());
    std::vector<int> tree_ids(q);
    for (int i = 0; i < q; ++i) {
        tree_ids[used_nums[i].second] = i;
    }

    Fenwick_tree tree(q);
    for (int i = 0; i < q; ++i) {
        auto [type, x] = actions[i];
        if (type == '+') {
            tree.add(tree_ids[i], x);
        } else {
            cout << tree.seg_sum(0, tree_ids[i]) << '\n';
        }
    }

    return 0;
}