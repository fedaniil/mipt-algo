/* Задача B. Знакочередование

Реализуйте структуру данных из n элементов a_1,a_2…a_n, поддерживающую следующие операции:
- присвоить элементу a_i значение j;
- найти знакочередующуюся сумму на отрезке от l до r включительно (a_{l}−a_{l+1}+a_{l+2}−…±a_{r})

* Входные данные

В первой строке входного файла содержится натуральное число n(1≤n≤1e5) — длина массива.
Во второй строке записаны начальные значения элементов (неотрицательные целые числа, не превосходящие 1e4)
В третьей строке находится натуральное число m(1≤m≤1e5) — количество операций.
В последующих m строках записаны операции:
- операция первого типа задается тремя числами 0 i j (1≤i≤n, 1≤j≤1e4)
- операция второго типа задается тремя числами 1 l r (1≤l≤r≤n)

* Выходные данные

Для каждой операции второго типа выведите на отдельной строке соответствующую знакочередующуюся сумму.
*/

#include <iostream>
#include <vector>

class Fenwick_tree {
    size_t sz = 0;
    std::vector<int> arr, pool;

    void add(size_t i, int d) {
        while (i < sz) {
            pool[i] += d;
            i |= (i + 1);
        }
    }

    int sum(int i) {
        int res = 0;
        while (i >= 0) {
            res += pool[i];
            i &= (i + 1);
            --i;
        }
        return res;
    }

public:

    Fenwick_tree(std::vector<int>& init): sz(init.size()), arr(move(init)), pool(sz) {
        for (size_t i = 0; i < sz; ++i) {
            add(i, (i % 2) ? -arr[i] : arr[i]);
        }
    }

    void set(int i, int x) {
        int diff = x - arr[i];
        arr[i] = x;
        add(i, (i % 2) ? -diff : diff);
    }

    int seg_sum(int i, int j) {
        return ((i % 2) ? -1 : 1) * (sum(j) - sum(i - 1));
    }
};

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n;
    cin >> n;
    std::vector<int> arr(n);
    for (int& x : arr) cin >> x;
    Fenwick_tree tree(arr);

    int m;
    cin >> m;
    int type, a, b;
    for (int i = 0; i < m; ++i) {
        cin >> type >> a >> b;
        if (type == 0) {
            tree.set(a - 1, b);
        } else {
            cout << tree.seg_sum(a - 1, b - 1) << '\n';
        }
    }

    return 0;
}