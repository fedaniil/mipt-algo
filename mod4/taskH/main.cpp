/* Задача H. Вторая статистика (RMQ)

Дано число N и последовательность из N целых чисел. Найти вторую порядковую статистику на заданных диапазонах.

Для решения задачи используйте структуру данных Sparse Table. Требуемое время обработки каждого диапазона O(1).
Время подготовки структуры данных O(nlogn)

* Входные данные

В первой строке заданы 2 числа: размер последовательности N и количество диапазонов M.
Следующие N целых чисел задают последовательность.
Далее вводятся M пар чисел - границ диапазонов.

* Выходные данные

Для каждого из M диапазонов напечатать элемент последовательности - 2ю порядковую статистику. По одному числу в строке.
*/

#include <iostream>
#include <vector>
#include <algorithm>

template<typename T>
std::pair<T, T> get_2_mins(const std::initializer_list<T>& lst) {
    T mn1 = std::min(lst), mn2;
    bool mn2_initialized = false;
    for (T x : lst) {
        if (x != mn1 && (x < mn2 || !mn2_initialized)) {
            mn2 = x;
            mn2_initialized = true;
        }
    }
    return {mn1, mn2};
}

class Sparse_table {
public:

    Sparse_table(const std::vector<int>& arr) : sz(arr.size()),
                                                min1(std::__lg(sz) + 1, std::vector<std::pair<int, int>>(sz)),
                                                min2(min1) {
        build(arr);
    }

    int get_seg_2nd_min(int l, int r);

private:

    size_t sz = 0;
    std::vector<std::vector<std::pair<int, int>>> min1, min2;

    void build(const std::vector<int>& arr);
};

inline int Sparse_table::get_seg_2nd_min(int l, int r) {
    int layer = std::__lg(r - l - 1);
    int layer_sz = 1 << layer;
    return get_2_mins({min1[layer][l], min1[layer][r - layer_sz],
                       min2[layer][l], min2[layer][r - layer_sz]}).second.first;
}

void Sparse_table::build(const std::vector<int>& arr) {
    for (int i = 0; i < sz; ++i) {
        min1[0][i] = {arr[i], i};
        min2[0][i] = {arr[i], i};
    }
    for (int l = 1; l <= std::__lg(sz); l++) {
        for (int i = 0; i + (1 << l) <= sz; i++) {
            int prev_sz = 1 << (l - 1);
            std::tie(min1[l][i], min2[l][i]) = get_2_mins({min1[l - 1][i], min1[l - 1][i + prev_sz],
                                                           min2[l - 1][i], min2[l - 1][i + prev_sz]});
        }
    }
}

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n, m;
    cin >> n >> m;
    std::vector<int> arr(n);
    for (int& x : arr) cin >> x;
    Sparse_table sparse(arr);

    int l, r;
    for (int i = 0; i < m; ++i) {
        cin >> l >> r;
        cout << sparse.get_seg_2nd_min(l - 1, r) << '\n';
    }

    return 0;
}