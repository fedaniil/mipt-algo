/* Задача J. Рюкзак

Найдите максимальный вес золота, который можно унести в рюкзаке вместительностью S,
если есть N золотых слитков с заданными весами.

* Входные данные

В первой строке входного файла запианы два числа — S и N (1 ⩽ S ⩽ 1e4, 1 ⩽ N ⩽ 300).
Далее следует N неотрицательных целых чисел, не превосходящих 1e5 — веса слитков.

* Выходные данные

Выведите искомый максимальный вес.
*/

#include <iostream>
#include <vector>

int find_max_payload(const std::vector<int>& w, int s) {
    int n = w.size();
    std::vector<std::vector<int>> dp(n + 1, std::vector<int>(s + 1));
    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j <= s; ++j) {
            dp[i][j] = dp[i - 1][j];
            if (j - w[i - 1] >= 0) {
                dp[i][j] = std::max(dp[i][j], dp[i - 1][j - w[i - 1]] + w[i - 1]);
            }
        }
    }

    return dp[n][s];
}

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int s, n;
    cin >> s >> n;
    std::vector<int> w(n);
    for (int &x : w) cin >> x;

    cout << find_max_payload(w, s) << '\n';

    return 0;
}