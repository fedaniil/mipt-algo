/* Задача B. Невозрастающая подпоследовательность

Вам требуется написать программу, которая по заданной последовательности находит максимальную невозрастающую
её подпоследовательность (т.е такую последовательность чисел a_i_1, a_i_2, …, a_i_k (i_1 < i_2 < … < i_k), что
a_i_1 ≥ a_i_2 ≥ … ≥ a_i_k и не существует последовательности с теми же свойствами длиной k + 1).

* Входные данные

В первой строке задано число n — количество элементов последовательности (1 ≤ n ≤ 239017).
В последующих строках идут сами числа последовательности a_i, отделенные друг от друга произвольным количеством
пробелов и переводов строки (все числа не превосходят по модулю 2^31 − 2).

* Выходные данные

Вам необходимо выдать в первой строке выходного файла число k — длину максимальной невозрастающей подпоследовательности.
В последующих строках должны быть выведены (по одному числу в каждой строке) все номера элементов
исходной последовательности i_j, образующих искомую подпоследовательность. Номера выводятся в порядке возрастания.
Если оптимальных решений несколько, разрешается выводить любое.
*/

#include <iostream>
#include <vector>
#include <algorithm>

const unsigned int INF = (1u << 31u) - 1;

std::vector<int> find_non_increasing_subsequence(const std::vector<int>& sequence) {
    int n = sequence.size();
    std::vector<int> dp(n + 1, INF), cur(n + 1), prev(n);
    dp[0] = -INF;
    cur[0] = prev[0] = -1;
    int ans_size = 0;
    for (int i = 0; i < n; ++i) {
        int x = sequence[i];
        int x_pos = std::upper_bound(dp.begin(), dp.end(), -x) - dp.begin();
        dp[x_pos] = -x;
        cur[x_pos] = i;
        prev[i] = cur[x_pos - 1];
        ans_size = std::max(ans_size, x_pos);
    }

    std::vector<int> ans;
    int pos = cur[ans_size];
    while (pos != -1) {
        ans.emplace_back(pos);
        pos = prev[pos];
    }
    std::reverse(ans.begin(), ans.end());

    return ans;
}

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n;
    cin >> n;
    std::vector<int> sequence(n);
    for (int& x : sequence) cin >> x;

    auto ans = find_non_increasing_subsequence(sequence);
    cout << ans.size() << '\n';
    for (int x : ans) {
        cout << x + 1 << '\n';
    }

    return 0;
}