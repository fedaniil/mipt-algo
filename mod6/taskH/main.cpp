/* Задача H. Наибольшая общая возрастающая

Даны две последовательности чисел — a и b. Нужно найти наибольшую общую возрастающую подпоследовательность.
Более формально: такие 1 ≤ i_1 < i_2 < ⋯ < i_k ≤ a_n и 1 ≤ j_1 < j_2 < ⋯ < j_k ≤ b_n, что
∀t: a_i_t = b_j_t, a_i_t < a_i_t + 1 и k максимально.

* Входные данные

На первой строке целые числа n и m от 1 до 3000 — длины последовательностей.
Вторая строка содержит n целых чисел, задающих первую последовательность.
Третья строка содержит m целых чисел, задающих вторуя последовательность.
Все элементы последовательностей — целые неотрицательные числа, не превосходящие 1e9.

* Выходные данные

Выведите одно целое число — длину наибольшей общей возрастающей подпоследовательности.
*/

#include <iostream>
#include <vector>

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int find_longest_common_increasing_sequence_length(const std::vector<int>& a, const std::vector<int>& b) {
    int n = a.size(), m = b.size();
    // dp[i][j] = max length of the sequence, ending in a[1..i - 1], b[j - 1]
    // note: dp is 1-indexed; a and b are 0-indexed, that is why "- 1" in a, b
    std::vector<std::vector<int>> dp(n + 1, std::vector<int>(m + 1));
    for (int i = 1; i <= n; ++i) {
        int best = 0;  // max dp[i - 1][0..j] where a[i - 1] > b[j - 1]
        for (int j = 1; j <= m; ++j) {
            dp[i][j] = dp[i - 1][j];  // at least dp[i - 1][j]
            // If elements compare equal, best + 1 is also an option
            // The idea is, normally you would have looped k = 1..j
            // searching for b[k - 1] < b[j - 1] with "best" (max) dp[i - 1][k]
            // but the a[i - 1] is fixed while looping j, so we could keep
            // best variable, keeping this "best" value on 1..j.
            // But what about "< b[j - 1]"? Trick: the variable is needed only
            // when a[i - 1] == b[j - 1], so when the condition is needed
            // b[k - 1] < b[j - 1] <=> b[k - 1] < a[i - 1]
            if (a[i - 1] == b[j - 1] && dp[i][j] < best + 1) {
                dp[i][j] = best + 1;
            }
            // update best
            if (a[i - 1] > b[j - 1] && dp[i - 1][j] > best) {
                best = dp[i - 1][j];
            }
        }
    }

    int ans = 0;
    for (int x : dp[n])  // ans is max from dp[n]
        ans = std::max(ans, x);
    return ans;
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int n, m;
    cin >> n >> m;
    std::vector<int> a(n), b(m);
    for (int& x : a) cin >> x;
    for (int& x : b) cin >> x;

    cout << find_longest_common_increasing_sequence_length(a, b) << '\n';

    return 0;
}