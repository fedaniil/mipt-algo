/* Задача L. Симпатичные узоры — 3

Компания BrokenTiles планирует заняться выкладыванием во дворах у состоятельных клиентов узор из черных и белых плиток,
каждая из которых имеет размер 1×1 метр. Известно, что дворы всех состоятельных людей имеют наиболее модную на сегодня
форму прямоугольника M×N метров.
Однако при составлении финансового плана у директора этой организации появилось целых две серьезных проблемы:
во первых, каждый новый клиент очевидно захочет, чтобы узор, выложенный у него во дворе, отличался от узоров всех
остальных клиентов этой фирмы, а во вторых, этот узор должен быть симпатичным.
Как показало исследование, узор является симпатичным, если в нем нигде не встречается квадрата 2×2 метра, полностью
покрытого плитками одного цвета.
Для составления финансового плана директору необходимо узнать, сколько клиентов он сможет обслужить, прежде чем
симпатичные узоры данного размера закончатся. Помогите ему!

* Входные данные

На первой строке входного файла находятся три натуральных числа n, m, mod. 1 ≤ n ≤ 1e100, 1 ≤ m ≤ 6, 1 ≤ mod ≤ 1e4.
n, m — размеры двора. mod — модуль, по которому нужно посчитать ответ.

* Выходные данные

Выведите в выходной файл единственное число — количество различных симпатичных узоров, которые можно выложить во дворе
размера n×m по модулю mod. Узоры, получающиеся друг из друга сдвигом, поворотом или отражением считаются различными.
*/

#include <iostream>
#include <vector>

class BigInteger {
public:

    BigInteger() = default;

    BigInteger(const BigInteger& big_n) = default;

    BigInteger(long long n): number(std::to_string(std::abs(n))), inv(n < 0) {}

    BigInteger& operator=(BigInteger big_n) {
        swap(big_n);
        return *this;
    }

    explicit operator bool() const {
        return number != "0";
    }

    std::string toString() const {
        return inv ? '-' + number : number;
    }

    int count_digits() const {
        return number.size();
    }

    int get_digit(size_t i) const {
        return number[i] - '0';
    }

    BigInteger operator+() const {
        return *this;
    }

    BigInteger operator-() const;

    BigInteger& operator+=(const BigInteger& big_n);

    BigInteger& operator-=(const BigInteger& big_n);

    BigInteger& operator++() {
        return *this += 1;
    }

    BigInteger operator++(int);

    BigInteger& operator--() {
        return *this -= 1;
    }

    BigInteger operator--(int);

    BigInteger& operator*=(const BigInteger& big_n);

    BigInteger& operator/=(const BigInteger& big_n) {
        divmod(big_n, "div");
        return *this;
    }

    BigInteger& operator%=(const BigInteger& big_n) {
        divmod(big_n, "mod");
        return *this;
    }

    // для удобства
    int mod10() const {
        return get_digit(number.size() - 1);
    }

    // для уменьшения кол-ва друзей; для быстрого сравнения с 0
    bool get_inv() const {
        return inv;
    }

    friend std::istream& operator>>(std::istream&, BigInteger&);  // вынужденный друг
    friend BigInteger operator"" _big(const char*, size_t);  // так сильно удобнее

private:

    std::string number = "0";
    bool inv = false;

    // для удобства
    BigInteger(std::string s, bool inv = false): number(std::move(s)), inv(inv) {
        remove_leading_zeros();
    }

    void swap(BigInteger& big_n) {
        std::swap(number, big_n.number);
        std::swap(inv, big_n.inv);
    }

    void remove_leading_zeros();

    // выполняет деление с остатком, записывает по месту результат деления/остаток (mode == "div" / "mod" соотв)
    void divmod(const BigInteger& big_n, const std::string& mode);
};

bool operator<(const BigInteger& a, const BigInteger& b) {
    if (a.get_inv() && b.get_inv()) {  // -|a| < -|b|
        return -b < -a;  // |b| < |a|
    } else if (!a.get_inv() && b.get_inv()) {  // |a| < -|b|
        return false;
    } else if (a.get_inv() && !b.get_inv()) {  // -|a| < |b|
        return true;
    }
    // |a| < |b|
    if (a.count_digits() == b.count_digits()) {
        for (int i = 0; i < a.count_digits(); ++i) {
            if (a.get_digit(i) < b.get_digit(i)) {
                return true;
            } else if (a.get_digit(i) > b.get_digit(i)) {
                return false;
            }
        }
        return false;
    }
    return a.count_digits() < b.count_digits();
}

inline bool operator>(const BigInteger& a, const BigInteger& b) {
    return b < a;
}

inline bool operator<=(const BigInteger& a, const BigInteger& b) {
    return !(a > b);
}

inline bool operator>=(const BigInteger& a, const BigInteger& b) {
    return !(a < b);
}

inline bool operator==(const BigInteger& a, const BigInteger& b) {
    return !(a < b || a > b);
}

inline bool operator!=(const BigInteger& a, const BigInteger& b) {
    return !(a == b);
}

BigInteger BigInteger::operator-() const {
    BigInteger res(*this);
    if (res.number != "0") {
        res.inv ^= true;
    }
    return res;
}

BigInteger operator+(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res += b;
    return res;
}

BigInteger operator-(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res -= b;
    return res;
}

BigInteger operator*(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res *= b;
    return res;
}

BigInteger operator/(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res /= b;
    return res;
}

BigInteger operator%(const BigInteger& a, const BigInteger& b) {
    BigInteger res(a);
    res %= b;
    return res;
}

BigInteger& BigInteger::operator+=(const BigInteger& big_n) {
    if (inv) {  // -|this| += big_n
        return *this = -(-*this - big_n);  // this = -(|this| - big_n)
    } else if (!inv && big_n.inv) {  // |this| += -|big_n|
        return *this -= -big_n;  // this -= |big_n|
    }
    // |this| += |big_n|
    std::string res(std::max(number.size(), big_n.number.size()) + 1, '0');
    int pos1 = number.size() - 1, pos2 = big_n.number.size() - 1, writepos = res.size() - 1;
    for (; writepos > 0; --pos1, --pos2, --writepos) {
        short digit1 = pos1 >= 0 ? number[pos1] - '0' : 0;
        short digit2 = pos2 >= 0 ? big_n.number[pos2] - '0' : 0;
        res[writepos] += digit1 + digit2;
        if (res[writepos] > '9') {
            res[writepos - 1] += 1;
            res[writepos] -= 10;
        }
    }
    number = res;
    remove_leading_zeros();
    return *this;
}

BigInteger& BigInteger::operator-=(const BigInteger& big_n) {
    if (inv) {  // -|this| -= big_n
        return *this = -(-*this + big_n);  // this = -(|this| + big_n)
    } else if (!inv && big_n.inv) {  // |this| -= -|big_n|
        return *this += -big_n;  // this += |big_n|
    }
    // |this| -= |big_n|
    if (big_n > *this) {
        return *this = -(big_n - *this);
    }
    bool taken_one = false;
    for (int pos1 = number.size() - 1, pos2 = big_n.number.size() - 1; pos1 >= 0; --pos1, --pos2) {
        short digit1 = number[pos1] - '0';
        short digit2 = pos2 >= 0 ? big_n.number[pos2] - '0' : 0;
        if (taken_one && digit1 > 0) {
            --digit1;
            taken_one = false;
        } else if (taken_one) {
            digit1 += 9;
        }
        number[pos1] = '0' + (digit1 - digit2);
        if (number[pos1] < '0') {
            number[pos1] += 10;
            taken_one = true;
        }
    }
    remove_leading_zeros();
    return *this;
}

BigInteger BigInteger::operator++(int) {
    BigInteger copy(*this);
    *this += 1;
    return copy;
}

BigInteger BigInteger::operator--(int) {
    BigInteger copy(*this);
    *this -= 1;
    return copy;
}

BigInteger& BigInteger::operator*=(const BigInteger& big_n) {
    std::string res(number.size() + big_n.number.size() + 1, '0');
    for (size_t pos2 = 0, shift = big_n.number.size() - 1; pos2 < big_n.number.size(); ++pos2, --shift) {
        int digit2 = big_n.number[pos2] - '0';
        int plus_next = 0;
        int writepos = res.size() - shift - 1;
        for (int pos1 = number.size() - 1; pos1 >= 0; --pos1, --writepos) {
            int digit1 = number[pos1] - '0';
            int sum = (res[writepos] - '0') + digit1 * digit2 + plus_next;
            res[writepos] = '0' + sum % 10;
            plus_next = sum / 10;
        }
        while (plus_next != 0) {
            int sum = (res[writepos] - '0') + plus_next;
            res[writepos] = '0' + sum % 10;
            plus_next = sum / 10;
            --writepos;
        }
    }
    number = res;
    remove_leading_zeros();
    inv ^= big_n.inv;
    if (number == "0") {
        inv = false;
    }
    return *this;
}

void BigInteger::remove_leading_zeros() {
    size_t fir = 0;
    while (fir < number.size() && number[fir] == '0') {
        ++fir;
    }
    if (fir == number.size()) {
        number = "0";
    } else {
        number = number.substr(fir);
    }
}

inline BigInteger abs(const BigInteger& big_n) {
    return big_n.get_inv() ? -big_n : big_n;
}

void BigInteger::divmod(const BigInteger& big_n, const std::string& mode) {
    size_t next_pos = big_n.number.size();
    std::string cur_mod = number.substr(0, next_pos), cur_div("0");
    while (true) {
        // в сравнениях строки и BigInteger используется приватный неявный каст строки к BigInteger
        while (next_pos < number.size() && cur_mod < abs(big_n)) {
            cur_mod += number[next_pos];
            cur_div += '0';
            ++next_pos;
        }
        if (cur_mod < abs(big_n)) { // получили остаток
            if (cur_mod.size() > 1) { // Если при этом хотя бы 1 раз дописывали к нему цифры
                // то нужно "разделить" его на big_n и получить ещё 1 ноль к результату деления
                cur_div += '0';
            }
            break;
        }
        int L = 1;
        int R = 10;
        while (R - L > 1) {
            int M = (R + L) / 2;
            if (abs(big_n) * M > cur_mod) {
                R = M;
            } else {
                L = M;
            }
        }
        cur_mod = (cur_mod - abs(big_n) * L).number; // здесь тоже используется неявный каст строки к BigInteger
        cur_div += '0' + L;
        if (next_pos < number.size()) {
            cur_mod += number[next_pos];
            ++next_pos;
        }
    }
    number = mode == "div" ? cur_div : cur_mod;
    if (mode == "div") {
        inv ^= big_n.inv;
    }
    if (number == "0") {
        inv = false;
    }
    remove_leading_zeros();
}

inline std::ostream& operator<<(std::ostream& out, const BigInteger& big_n) {
    return out << big_n.toString();
}

std::istream& operator>>(std::istream& in, BigInteger& big_n) {
    big_n = 0;
    std::string s;
    char c;
    do c = in.get(); while (isspace(c));  // пропустить ведущие пробелы
    // особая обработка первого символа
    bool inv = false;
    if (c == '-') {
        inv = true;
    } else {
        s = c;
    }
    // прочитать остальное
    for (c = in.get(); !in.eof() && !isspace(c); c = in.get()) {
        s += c;
    }

    big_n = BigInteger(s, inv);
    return in;
}

template<typename T>
class Matrix {
public:
    explicit Matrix(std::vector<std::vector<T>> arr) : matrix(std::move(arr)), n(matrix.size()), m(n ? matrix[0].size() : 0) {}

    Matrix(size_t n, size_t m, T val) : matrix(n, std::vector<T>(m, val)), n(n), m(m) {}

    Matrix(const Matrix& other): matrix(other.matrix), n(other.n), m(other.m) {}

    void swap(Matrix& other) {
        std::swap(matrix, other.matrix);
        std::swap(n, other.n);
        std::swap(m, other.m);
    }

    Matrix& operator=(Matrix other) {
        swap(other);
        return *this;
    }

    static Matrix one(size_t n) {
        Matrix res(n, n, 0);
        for (int i = 0; i < n; ++i) {
            res[i][i] = 1;
        }
        return res;
    }

    void transpose() {
        Matrix new_matrix(m, n, 0);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                new_matrix[j][i] = matrix[i][j];
            }
        }
        *this = new_matrix;
    }

    const std::vector<T>& operator[](size_t i) const {
        return matrix[i];
    }

    std::vector<T>& operator[](size_t i) {
        return matrix[i];
    }

    explicit operator T() const {
        return matrix[0][0];
    }

    Matrix& operator*=(const Matrix& other) {
        Matrix res(n, other.m, 0);

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < other.m; ++j) {
                for (int k = 0; k < m; ++k) {
                    res[i][j] += matrix[i][k] * other[k][j];
                }
            }
        }

        return *this = res;
    }

    Matrix pow(const BigInteger& p) const {
        if (p == 0) {
            return one(n);
        } else {
            Matrix res = pow(p / 2);
            return p.mod10() % 2 == 0 ? res * res : *this * res * res;
        }
    }

private:
    std::vector<std::vector<T>> matrix;
    size_t n, m;
};

template<typename T>
Matrix<T> operator*(const Matrix<T>& a, const Matrix<T>& b) {
    Matrix<T> res = a;
    res *= b;
    return res;
}

template<typename T>
class Modulus {
public:
    static long long mod;

    Modulus(T x) : num(x) {
        normalise();
    }

    Modulus() : Modulus(0) {}

    Modulus& operator=(Modulus other) {
        std::swap(num, other.num);
        return *this;
    }

    explicit operator T() const {
        return num;
    }

    void normalise() {
        num %= mod;
        while (num < 0) {
            num += mod;
        }
    }

    Modulus& operator+=(const Modulus& other) {
        num += other.num;
        normalise();
        return *this;
    }

    Modulus& operator-=(const Modulus& other) {
        num -= other.num;
        normalise();
        return *this;
    }

    Modulus& operator*=(const Modulus& other) {
        num *= other.num;
        normalise();
        return *this;
    }

    Modulus& operator/=(const Modulus& other) {
        num /= other.num;
        normalise();
        return *this;
    }

private:
    T num;
};

template<typename T>
Modulus<T> operator+(Modulus<T> a, const Modulus<T>& b) {
    a += b;
    return a;
}

template<typename T>
Modulus<T> operator-(Modulus<T> a, const Modulus<T>& b) {
    a -= b;
    return a;
}

template<typename T>
Modulus<T> operator*(Modulus<T> a, const Modulus<T>& b) {
    a *= b;
    return a;
}

template<typename T>
Modulus<T> operator/(Modulus<T> a, const Modulus<T>& b) {
    a /= b;
    return a;
}

template<typename T>
std::istream& operator>>(std::istream& in, Modulus<T>& x) {
    T val;
    in >> val;
    x = val;
    return in;
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const Modulus<T>& x) {
    return out << static_cast<T>(x);
}

template<>
long long Modulus<long long>::mod = 1e9 + 7;

bool is_valid(size_t mask1, size_t mask2, size_t sz) {
    for (size_t bit = 2, prev_bit = 1; bit < (1u << sz); bit <<= 1u, prev_bit <<= 1u) {
        size_t a = mask1 & bit, b = mask2 & bit,
               c = mask1 & prev_bit, d = mask2 & prev_bit;
        if (a == b && c == d && a == (c << 1u)) {
            return false;
        }
    }
    return true;
}

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

Modulus<long long> count_pretty_patterns(const BigInteger& n, unsigned int m) {
    std::vector<std::vector<Modulus<long long>>> dp(1u << m, std::vector<Modulus<long long>>(1u << m));
    for (size_t mask1 = 0; mask1 < (1u << m); ++mask1) {
        for (size_t mask2 = 0; mask2 < (1u << m); ++mask2) {
            dp[mask1][mask2] = static_cast<long long>(is_valid(mask1, mask2, m));
        }
    }

    Matrix<Modulus<long long>> dp_matrix(dp);
    Matrix<Modulus<long long>> base(1, (1u << m), 1);
    Matrix<Modulus<long long>> one_column((1u << m), 1, 1);

    dp_matrix.transpose();
    base *= dp_matrix.pow(n - 1);
    base *= one_column;

    return static_cast<Modulus<long long>>(base);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    BigInteger n;
    unsigned int m;
    cin >> n >> m >> Modulus<long long>::mod;

    cout << count_pretty_patterns(n, m) << '\n';

    return 0;
}
